class Url < ActiveRecord::Base
  has_many :stats
  
  IPv4_PART = /\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]/  # 0-255
  URL_REGEXP = %r{
    \A
    https?://                                        # http:// or https://
    ([^\s:@]+:[^\s:@]*@)?                            # optional username:pw@
    ( (xn--)?[^\W_]+([-.][^\W_]+)*\.[a-z]{2,6}\.? |  # domain (including Punycode/IDN)...
        #{IPv4_PART}(\.#{IPv4_PART}){3} )            # or IPv4
    (:\d{1,5})?                                      # optional port
    ([/?]\S*)?                                       # optional /whatever or ?whatever
    \Z
  }iux


  validates :original_url, 
            :presence => true, 
            :uniqueness => { :case_sensitive => false, :message => "This URL is already in our database" },
            :length => { :maximum => 2024 },
            :format => { :with => URL_REGEXP, :message => "Invalid URL" }
            

  validates :path, 
            :allow_blank => true, 
            :uniqueness => { :case_sensitive => false, :message => "Custom path has already been taken" }, 
            :length => { :maximum => 20, :minimum => 2, :message => "Custom path must be between 2 and 20 characters"},
            :format => { :with => /^[-_a-z0-9]+$/i, :message => "Invalid Path" }


  def self.random_path
    Anybase::Base62.random(path_size).downcase
  end
  
  def self.path_size
    Url.count.to_s.length.to_i-1
  end

  def self.delete!
    self.destroy
  end

end
