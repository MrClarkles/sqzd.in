class ApiController < ApplicationController

  PATH_EXCLUSIONS = ['static','api','index','latest_urls','clark','admin','create','update','delete','destroy']
  URL_EXCLUSIONS = []

  def index

    logger.info("\n\n\n params: "+params.inspect+"\n\n\n")
    redirect_to :action => :show   if (params[:path] && params[:path].include?('+'))
    redirect_to :action => :expand if params[:path] and return

    @meta_title = 'Index'
    @body_class = 'index_wrap'
  end

  def create
    404 unless request.post?

    json = {:result => nil}

    url  = params["opts"]["url"]

    begin
      parsed_url = URI.parse(url)
    rescue URI::InvalidURIError => err
      json[:errors] = {"original_url" => "URL is invalid: #{err}"}
    end

    if parsed_url.scheme && parsed_url.host
      path = params["opts"]["path"] 
      path = Url.random_path if path.blank?
      
      unless PATH_EXCLUSIONS.include?(path)
        json[:url] = url
        json[:path] = path
        
        @url = Url.new(:original_url => url, :path => path)

        begin
          json[:result] = @url.save! 
        rescue ActiveRecord::RecordInvalid => invalid
          json[:errors] = invalid.record.errors
          json[:result] = "errors"
        end
      else
        json[:errors] = {"path" => "This custom path is reserved, please try another"}
        json[:result] = "errors"
      end
    else
      json[:errors] = {"original_url" => "This URL is invalid"}
      json[:result] = "errors"
    end
    render :json => json
  end

  def expand
    404 unless params[:path]

    url = Url.find_by_path(params[:path])
    Url.increment_counter(:hits, url.id)
    url.save

    stat = update_stats(url)

    redirect_to url["original_url"] and return
  end

  def show
    @body_class = 'show_wrap'
    path = params["path_plus"].gsub!(/\+/,'')
    url = Url.find_by_path(path)
    render :show, :locals => { :url => url, :stats => url.stats.all }
  end

  def update
    404 unless request.post? && params["record"]

  end

  def delete
    404 unless request.post? && params["record"]
  end

  def latest_urls
    limit = (params[:limit]) ? params[:limit] : 10
    urls = []

    Url.find(:all, :limit => limit, :order => 'created_at desc').each do |url|
      urls << {:id=> url.id,:hits=>url.hits,:original_url=>url.original_url,:path=>url.path,:created_at=>url.created_at}
    end

    respond_to do |format|
      format.json { render :json => urls } 
      format.html { render :partial => "latest_urls", :locals => {:latest_urls => urls} }
    end
    
  end

  def update_stats(url={})
    entry = Stat.new do |s|
      s.url_id       = url.id
      s.referrer     = request.env["HTTP_REFERER"]
      s.user_agent   = request.env["HTTP_USER_AGENT"]
      s.country_code = country_code(url.ip)
      s.ip_address   = request.env["REMOTE_ADDR"]
    end
    entry.save
    return true
  end

  def country_code(ip)
    require "net/http"
    require "uri"
    uri = URI.parse("http://api.hostip.info/country.php?ip=#{ip}")
    response = Net::HTTP.get_response(uri)
    cc = response.body.to_s
    cc
  end

  protected

end
