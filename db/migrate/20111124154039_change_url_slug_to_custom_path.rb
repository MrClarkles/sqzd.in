class ChangeUrlSlugToCustomPath < ActiveRecord::Migration
  def self.up
    change_table :urls do |t|
      t.rename :slug, :custom_path
    end
  end

  def self.down
    change_table :urls do |t|
      t.rename :custom_path, :slug
    end
  end
end
