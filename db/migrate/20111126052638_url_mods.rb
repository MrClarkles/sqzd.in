class UrlMods < ActiveRecord::Migration
  def self.up
    rename_column('urls', 'custom_path', 'path')
  end

  def self.down
    rename_column('urls', 'path', 'custom_path')
  end
end
