class AddYourlsDataToUrl < ActiveRecord::Migration
  def self.up
    add_column 'urls', 'title', :string
    add_column 'urls', 'ip',    :string
  end

  def self.down
    remove_column 'urls', 'title'
    remove_column 'urls', 'ip'
  end
end
