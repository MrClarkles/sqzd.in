class ChangeNullableColumns < ActiveRecord::Migration
  def self.up
    change_column_null('urls', 'id', :null => false)
    change_column_null('urls', 'hits', :default => 0)
    change_column_null('urls', 'created_at', :null => false)
  end

  def self.down
    change_column_null('urls', 'id', :null => true)
    change_column_null('urls', 'hits', :default => nil)
    change_column_null('urls', 'created_at', :null => true)
  end
end
