class ChangeHitsDefault < ActiveRecord::Migration
  def self.up
    change_column_default('urls', 'hits', 0)
  end

  def self.down
    change_column_default('urls', 'hits', nil)
  end
end
