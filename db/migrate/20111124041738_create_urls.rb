class CreateUrls < ActiveRecord::Migration
  def self.up
    create_table :urls do |t|
      
      t.text :original_url
      t.string :slug
      t.integer :id, :hits

      t.timestamps
    end
  end

  def self.down
    drop_table :urls
  end
end
