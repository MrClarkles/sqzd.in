class ModifyStatsSchema < ActiveRecord::Migration
  def self.up
    remove_column :stats, :ip
    remove_column :stats, :updated_at
    rename_column :stats, :browser, :user_agent
    add_column :stats, :country_code, :string
  end

  def self.down
    add_column :stats, :ip
    add_column :stats, :updated_at
    rename_column:stats, :user_agent, :browser
    remove_column :stats, :country_code
  end
end
