class AddIpColumn < ActiveRecord::Migration
  def self.up
    add_column :stats, :ip_address, :string
  end

  def self.down
    remove_column :stats, :ip_address
  end
end
