Sqzdin::Application.routes.draw do
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)


  match '/static/about/?'    => 'static#about',   :as => :about
  match '/static/privacy/?'  => 'static#privacy', :as => :privacy
  match '/static/tos/?'      => 'static#tos',     :as => :tos

  match '/api/latest_urls' => 'api#latest_urls',  :as => :latest_urls
  match '/api/create'      => 'api#create',  :via => :post, :as => :create
  match '/api/update/:id'  => 'api#update',  :via => :post, :as => :update
  match '/api/delete/:id'  => 'api#delete',  :via => :post, :as => :delete
  
  match '/admin'           => 'admin#index'
  
  match '/:path_plus'   => 'api#show',   :via => :get, :requirements => {:path_plus => /\+$/}
  match '/:path'   => 'api#expand', :via => :get
  match '/'        => 'api#index',  :via => :get



  root :to => "api#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
